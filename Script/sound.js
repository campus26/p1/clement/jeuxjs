export function toucheSound() {
    let touchSound = new Audio('./sound/tuer.wav');
    touchSound.play();
}

export function tirSound() {
    let tirSound = new Audio('./sound/feu.wav');
    tirSound.play();
}

export function sautSound() {
    let sautSound = new Audio('./sound/sauter.wav');
    sautSound.play();
}

export function music() {
    let fontSound = new Audio('./sound/wakfuRubilax.wav');
    fontSound.play();
}