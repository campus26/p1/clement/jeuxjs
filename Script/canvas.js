import { engine, render } from "./InitMatter"
import { map } from "./block"
import { barre } from "./barre"
import { Personnage } from "./Personnage"
import { score } from "./score"
import { zone } from "./zonne"
import { tir } from "./tir"
import { toucheSound } from "./sound"
import { tirSound } from "./sound"
import { music } from "./sound"
import { confetti } from "./confetti"
//on ajust la fenetre si on resize



var largeur = 1720;
var hauteur = 780;

let nbKillJ1 = 0
let nbKillJ2 = 0

//active le le ficher block
map();


let rngPosition = Math.floor(Math.random() * 5);
let cptrng = 4;


//pour conteste la zonne
let ZonnePerso1 = false;
let ZonnePerso2 = false;



let zonne = [{ x: 850, y: 600 }, { x: 850, y: 250 }, { x: 1200, y: 250 }, { x: 200, y: 600 }, { x: 1025, y: 750 }];

zone(zonne[rngPosition]);



let Barre = new barre(zonne[rngPosition].x, zonne[rngPosition].y - 195, "red");
let Barre2 = new barre(zonne[rngPosition].x, zonne[rngPosition].y - 165, "blue");

//creation des personnages (postion x y largeur hauteur nom du labal)
let perso1 = new Personnage(90, hauteur - 10, 50, 100, "perso1", "Girl");
let perso2 = new Personnage(1900, hauteur - 10, 50, 100, "perso2", "Girl2");



let Score1 = new score(100, 50);
let Score2 = new score(largeur - 100, 50);


//ecoute des touches
document.addEventListener("keydown", direction);
document.addEventListener("keyup", Stopdirection);



//objet qu'on voit dans la caméra
let ObjectCam = [perso1.Perso, perso2.Perso, Barre.barre, Barre2.barre];


//va rafraichire tout les 60hz et on doit la rappeler dans la fonction
perso1.afficheVie();
perso2.afficheVie();


window.requestAnimationFrame(deplacement);



//quand une valeur passe a true fait la fonction draw pour faire avance les cube
function deplacement() {


    //on actualise les donner de mouvenment exmple la gravité l'inertie du personnage
    perso1.mouvement();
    perso2.mouvement();


    if (Score1.getScore() == 3) {
        localStorage.setItem(localStorage.length, JSON.stringify({ name: document.getElementById("myname").value, Kill: nbKillJ1 }));
        localStorage.setItem(localStorage.length, JSON.stringify({ name: document.getElementById("myname1").value, Kill: nbKillJ2 }));
        confetti();
        ObjectCam.splice(1, 3);


    }

    if (Score2.getScore() == 3) {

        ObjectCam.splice(0, 1);
        ObjectCam.splice(1, 2);

        confetti();
        localStorage.setItem(localStorage.length, JSON.stringify({ name: document.getElementById("myname1").value, Kill: nbKillJ2 }));
        localStorage.setItem(localStorage.length, JSON.stringify({ name: document.getElementById("myname").value, Kill: nbKillJ1 }));

    }


    Render.lookAt(render, ObjectCam, { x: 150, y: 100 }, true);

    window.requestAnimationFrame(deplacement);

}


//quand on met key press on met les variable en true pour actioner ne mouvent
function direction(e) {

    switch (e.key) {

        //pour lancer la music de fond 
        case "m":
            music();
            break;

        case "z":
            perso1.jump = true;

            break;
            //on set le status du perso pour savoir ou tire (gauche ou droite)
        case "q":
            perso1.gauche = true;
            perso1.status = "gauche";

            break;

        case "d":
            perso1.droite = true;
            perso1.status = "droite";
            break;

        case "s":
            perso1.reculer = true;
            break;

        case "ArrowUp":
            perso2.jump = true;

            break;

        case "ArrowLeft":
            perso2.gauche = true;
            perso2.status = "gauche";

            break;

        case "ArrowRight":
            perso2.droite = true;
            perso2.status = "droite";

            break;

        case "ArrowDown":
            perso2.reculer = true;
            break;

            //la touche espace
        case " ":
            if (perso1.canShoot) {
                //on set la directoion du shoot selon le status du personnage (drot ou gauche)
                perso1.startDelayShoot();
                let shoot = new tir(perso1.GetX(), perso1.GetY(), 10, perso1);
                tirSound();

            }
            break;

        case "0":
            if (perso2.canShoot) {
                perso2.startDelayShoot();
                let shoot = new tir(perso2.GetX(), perso2.GetY(), 10, perso2);
                tirSound();
            }
            break;


        default:
            break;
    }

}


//quans on relache la touche la valeur ce met en fasle pour stoper le mouvement
function Stopdirection(e) {



    switch (e.key) {
        case "z":
            perso1.jump = false;
            break;

        case "q":
            perso1.gauche = false;
            break;

        case "d":
            perso1.droite = false;
            break;

        case "s":
            perso1.reculer = false;
            perso1.onBas = true;
            Body.scale(perso1.Perso, 1, 2);
            Body.setInertia(perso1.Perso, 'Infinity');

            break;

        case "ArrowUp":
            perso2.jump = false;

            break;

        case "ArrowLeft":
            perso2.gauche = false;
            break;

        case "ArrowRight":
            perso2.droite = false;
            break;

        case "ArrowDown":
            perso2.reculer = false;
            perso2.onBas = true;
            Body.scale(perso2.Perso, 1, 2);
            Body.setInertia(perso2.Perso, 'Infinity');
            break;


        default:
            break;
    }

}



//event de colision
Events.on(engine, 'collisionStart', function(event) {
    var pairs = event.pairs;

    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i];


        //suprime le tire apres avoir touché mais ne doit pas ce suprimer dans la colisoin de l'arene
        if ((pairs[i].bodyB.label == "moBullet") && (pairs[i].bodyA.label !== "arene")) {
            World.remove(engine.world, pair.bodyB);


            if (pairs[i].bodyA.label == "perso1") {
                perso1.removeLife()
                if (perso1.vie == 0) {
                    Body.setPosition(pairs[i].bodyA, { x: 90, y: hauteur - 30 });
                    perso1.vie = 3;
                    perso1.afficheVie();
                    toucheSound();
                    nbKillJ2++;
                }


            }
            if (pairs[i].bodyA.label == "perso2") {
                perso2.removeLife()
                if (perso2.vie == 0) {
                    Body.setPosition(pairs[i].bodyA, { x: largeur - 90, y: hauteur - 30 })
                    perso2.vie = 3;
                    perso2.afficheVie();
                    toucheSound();
                    nbKillJ1++;

                }

            }


        }

        //detection quand on touche un label ground (sol mur)
        if ((pairs[i].bodyB.label == "perso1") && (pairs[i].bodyA.label == "ground")) {

            perso1.onJump = true;
        }



        if ((pairs[i].bodyB.label == "perso2") && (pairs[i].bodyA.label == "ground")) {
            perso2.onJump = true;
        }

    }
});

//ajout de point si il ce trouve dans la zone
Events.on(engine, 'collisionActive', function(event) {
    var pairs = event.pairs;


    for (var i = 0; i < pairs.length; i++) {

        //condition pour voir qui ne sont plus dans la zonne
        if (!ZonnePerso1 || !ZonnePerso2) {


            if ((pairs[i].bodyA.label == "arene") && (pairs[i].bodyB.label == "perso1")) {

                ZonnePerso1 = true;
                // si le pourcentage n'est pas arriver a 100 ça fait la foction setPourcent
                if (Barre.SetPourcent()) {

                    //déplacer la zonne de capture et ajouter un pts

                    Score1.setScore(true);

                    Barre.reset();
                    Barre2.reset();


                    zonne.splice(rngPosition, 1);
                    cptrng--;
                    rngPosition = Math.floor(Math.random() * cptrng)
                    Body.setPosition(pairs[i].bodyA, { x: zonne[rngPosition].x, y: zonne[rngPosition].y });
                    Body.setPosition(Barre.barre, { x: zonne[rngPosition].x, y: zonne[rngPosition].y - 195 });
                    Body.setPosition(Barre2.barre, { x: zonne[rngPosition].x, y: zonne[rngPosition].y - 165 });

                }

            }


            if ((pairs[i].bodyA.label == "arene") && (pairs[i].bodyB.label == "perso2")) {

                ZonnePerso2 = true;
                if (Barre2.SetPourcent()) {

                    Score2.setScore(false);

                    Barre.reset();
                    Barre2.reset();

                    zonne.splice(rngPosition, 1);
                    cptrng--;
                    rngPosition = Math.floor(Math.random() * cptrng)

                    Body.setPosition(pairs[i].bodyA, { x: zonne[rngPosition].x, y: zonne[rngPosition].y })
                    Body.setPosition(Barre.barre, { x: zonne[rngPosition].x, y: zonne[rngPosition].y - 195 });
                    Body.setPosition(Barre2.barre, { x: zonne[rngPosition].x, y: zonne[rngPosition].y - 165 });
                }
            }
        }




    }
});


// met a false quand il sort de la zonne
Events.on(engine, 'collisionEnd', function(event) {
    var pairs = event.pairs;

    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i];

        if ((pairs[i].bodyA.label == "arene") && (pairs[i].bodyB.label == "perso1")) {

            ZonnePerso1 = false;
        }

        if ((pairs[i].bodyA.label == "arene") && (pairs[i].bodyB.label == "perso2")) {
            ZonnePerso2 = false;
        }
    }
});